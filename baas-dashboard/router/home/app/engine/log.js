const router = require("koa-router")();
/**
 * api {get} /home/app/engine/log/:page 日志记录
 *
 * apiParam {Number} page 页码
 *
 */
router.get("/enginStatus", async (ctx, next) => {
  const baas = ctx.baas;
  const userBaas = await BaaS.Models.user_baas
    .query({ where: { baas_id: baas.id } })
    .fetch();
  Object.assign(baas, { engine: userBaas.engine });
  ctx.success(baas);
});
/**
 * api {get} /home/app/engine/switch 引擎开关
 *
 * apiParam {Number} engine 引擎开关（0关，1开）
 *
 */
router.get("/switch", async (ctx, next) => {
  const baas = ctx.baas;
  const engine = ctx.query.engine;
  const userBaas = await BaaS.Models.user_baas
    .forge({ baas_id: baas.id })
    .fetch();
  await BaaS.Models.user_baas.forge({ id: userBaas.id, engine: engine }).save();
  ctx.success("修改成功");
});
module.exports = router;
