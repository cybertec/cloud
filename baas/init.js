const redis = require("./init/redis");
const schedule = require("./init/schedule");
/**
 * 初始化
 */
(async () => {
  await redis();
})();
